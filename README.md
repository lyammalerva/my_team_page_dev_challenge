# Acerca de mí
¡Hola! 

Soy Andree, estoy creando mi portafolio en [devChallenges](https://devchallenges.io/), en conjunto a [gitlab](https://gitlab.com/andreemalerva/my_team_page_dev_challenge), actualmente soy Desarrollador Front End, y me agrada seguir aprendiento.

Trabajemos juntos, te dejo por acá mi contacto.
```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

Este es un proyecto de 'DevChallenge - My team page', cada commit representa una avance en HTML, CSS y/o CSS.

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://my-team-page-devchallenge-am.netlify.app//)🫶🏻

# Politícas de privacidad

Las imagenes, archivos css, html y adicionales son propiedad de ©2022 LYAM ANDREE CRUZ MALERVA
